<img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/11838498/logo.png?width=20" alt="logo" width="150"/>

# packdoc
CLI Tool to control package.json health, it can lint, pin versions via lock files, checking engines and check unused/missing dependencies 

## Why ?
Ive written this tool mainly as an Continous Integration helper, in my company we had several scripts for package health checks and i wanted to have everything in one CLI.  
By now i also focus more on daily **user** tasks simply to make **OUR** workflow easier.  
This project also dogfeeds itself as you can see here: https://gitlab.com/FreaKzero/packdoc/blob/master/.gitlab-ci.yml

## Install
npm:  
`npm install -g packdoc`  
yarn:  
`yarn global add packdoc`  

## Linting defaults
I wont "loosen" the linting defaults as are they now. Based on my experience and the usecase i have right now the best way to ensure the stuff "can be built" is that you fix your versions in the package.json regardless of package-lock.json or yarn.lock. I know this is highly opiononated so as i decided to publish the package for the public i ensured that this would be configureable - so please dont make issue requests for the standard linting.

## Engines Command
**When you want to use it**
  - You want to validate Alien Technologies which yarn wont resolve (The engine "XXX" appears to be invalid)
  - Giving fast help to your developers how to update certain engines
  - Using NPM and want to validate your engines

**Drawbacks**
  - In some cases when using yarn you have to ignore the engines validation

**Troubleshooting**
  - If you run into a incompatible module error with yarn  
run `packdoc engines --strict` first and then use yarn with `yarn --ignore-engines`  

  - My Alientechnology spits out more than a version string how can i validate ?  
see Plugin development  

**Which Engines are supported out of the box ?**  
See variable `enginesDefault` in https://gitlab.com/FreaKzero/packdoc/blob/master/src/settings.js  

## Plugin Development
Put a pluginfile named `packdoc-plugin.js` in your project root folder to activate plugins.  

### Engine Plugin
Feel free to make an issue or Merge request with your engine-plugins to see them integrated into this project.  
Output problem Example: `git --version` 
```
git version 2.11.0
```
Plugin:  
```
module.exports = {
  // plugin for engines
  engines: {
    // the bin which is used in package.json
    git: {
      // the parameter to use for outputting the version string
      param: '--version',
      // String transformer - the value will be sanitized after this step with semver.clean
      transform: (value) => value.split(' ')[2] || null,
      // the URL which will be shown when versioncheck fails
      url: 'https://git-scm.com/downloads'
    }
  }
}
```

### Depcheck Plugin
The `packdoc usage` command uses **depcheck** (https://github.com/depcheck/depcheck) internally.  
When you need a custom configuration for it you can use a plugin entry to overwrite the current behaviour.  
The properties `usageIgnore` and `usageIgnoreDirs` in your package json will not work anymore (because overwritten)  

Example: 

```
module.exports = {
  // plugin for depcheck
  depcheck: (depcheck, pkg) => ({
    // the depcheck library will be resolved in this callback so you dont have to require it
    // you can access properties of your package json via the pkg variable
    ignoreBinPackage: false,
    skipMissing: false,
    ignoreDirs: [],
    ignoreMatches: [],
    parsers: {
      '*.js': depcheck.parser.es6,
      '*.jsx': depcheck.parser.jsx
    },
    detectors: [
      depcheck.detector.requireCallExpression,
      depcheck.detector.importDeclaration
    ],
    specials: [depcheck.special.webpack, depcheck.special.babel]
  })
};
```

```
Usage

  $ packdoc usage
    Check unused and missing dependencies
  Options  
    --strict, -s         Strict, exits with 1 when missing or unused deps are found

  $ packdoc lint
    package.json linting
  Options  
    --strict, -s         Strict, exits with 1 when linting issues are found
    --list, -l           Show list of current applied linting rules

  $ packdoc fix
    Fix issues in package.json - omit all options to fix everything
  Options
    --pin, -p            Pins versions based on lockfile
    --sort, -s           Sorts dependencies alphabetically
    --install, -i        Install missing dependencies 

  $ packdoc audit
    Runs engine validation, usage and <manager> audit command
  Options
    --strict,s           Strict, exits with 1 when audit issues are found

  $ packdoc engines
    Check and give help for engines entry in package.json
    see ${pkg.repository} for plugin development
  Options
    --strict, -s         Strict, exits with 1 when engine issues are found
    --list, -l           Show list of registered engine validators (also plugins)

  $ packdoc publish <semver> --token=<token>
    CI helper to publish packages to a registry (always semver strict)
    semver check -> overwrite package.version -> generating ~/.npmrc -> npm publish
  Options  
    --token, -t          authToken for your registry (required)
    --registry, -r       Registry where to publish (default: //registry.npmjs.org)

  $ packdoc bump <semver>
    Tool for versioning (overwrite) package.json or checking semver without writing
  Options
    --check, -c          Check if given version is SEMVER conform without writing to package.json
    --loose, -l          loose versioning
  
  Setup Example

  {
    "packdoc": {
      // linting rules overwrite, default []
      // rule definition documentation: https://github.com/tclindner/npm-package-json-lint/wiki
      "lint": {'require-homepage': 'off'},
      // ignore false-positives for packdoc usage command, default []
      "usageIgnore": ["@babel/*", "*eslint*"],
      // ignore directories for packdoc usage command, default []
      "usageIgnoreDirs": ['build', 'dist'],
      // npm or yarn, default yarn
      "manager": "yarn"
    }
  }
```

## Changelog

### Version 0.0.7
- REFACTOR install flag now on fix command
- FEATURE check flag for bump
- REFACTOR usage and added 

### Version 0.1.0
- FEATURE Engine Checker and Helper
- FEATURE Plugin System (engines only)

### Version 1.0.0
- BREAK Remove --dir Flag for bump
- ADD engine checker for git
- FEATURE npm publish helper for ci systems

### Version 1.1.0
- BREAK --rules for lint is now --list
- FEATURE depcheck config plugin
- FEATURE --list flag for engines

### Version 1.2.0
- FEATURE package contents inected in depcheck plugin
- FEATURE directoryignore rule in packagejson for usage command

### Version 1.3.0
- FEATURE audit command