#!/bin/sh
LAST=$(git describe --tags --abbrev=0)
FILE="README.md"
if [ -z "$1" ]; then
  git log --pretty=format:%s "$LAST"..HEAD | grep -E "FIX|ADD|FEATURE|BREAK|REFACTOR" | awk '{print "- "$0}' >> "$FILE"
else 
  printf "\n" >> "$FILE"
  echo "### Version $1" >> "$FILE"
  git log --pretty=format:%s "$LAST"..HEAD | grep -E "FIX|ADD|FEATURE|BREAK|REFACTOR" | awk '{print "- "$0}' >> "$FILE"
fi

 