/* istanbul ignore file */
const chai = require('chai');
const mockery = require('mockery');
const sinon = require('sinon');

const expect = chai.expect;

const helpermock = {
  error: msg => msg,
  quit: msg => msg,
  writeJson: () => 1,
  log: msg => msg
};

// Because chai-spies suck with mockery....
const errorSpy = sinon.spy(helpermock, 'error');
const logSpy = sinon.spy(helpermock, 'log');

describe('routine/usage', () => {
  beforeEach(() => {
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    mockery.registerMock('../helpers', helpermock);
    mockery.registerMock('child_process');
  });
  afterEach(() => {
    errorSpy.resetHistory();
    logSpy.resetHistory();
    mockery.resetCache();
    mockery.deregisterAll();
  });

  it('printMissing() exits with 0 when no missing dependencies are found', () => {
    const { printMissing } = require('../src/routines/usage');

    const data = {};

    const pkg = {
      dependencies: {
        a: '1.1.1',
        b: '2.2.2'
      },
      resolutions: {
        c: '3.3.3'
      }
    };

    printMissing(data, pkg);

    expect(logSpy.getCall(0).args).eql(['Missing Dependencies', 'yellow']);
    expect(logSpy.getCall(1).args).eql([' No missing Dependencies found', 'green']);
  });

  it('printMissing() prints block based on depcheck wrapper data structure', () => {
    const { printMissing } = require('../src/routines/usage');

    const missing = [
      { dep: 'rly', paths: ['1/2/3'] },
      { dep: 'wat', paths: ['/a/b/c/d', 'e/f/g'] }
    ];

    printMissing(missing);

    expect(logSpy.getCall(0).args).eql(['Missing Dependencies', 'yellow']);
    expect(logSpy.getCall(1).args).eql(['  Missing dependency found: rly', 'red']);
    expect(logSpy.getCall(2).args).eql(['  - 1/2/3']);
    expect(logSpy.getCall(3).args).eql(['  Missing dependency found: wat', 'red']);
    expect(logSpy.getCall(4).args).eql(['  - /a/b/c/d']);
    expect(logSpy.getCall(5).args).eql(['  - e/f/g']);
  });

  it('printUnused() prints block based on depcheck data structure', () => {
    const { printUnused } = require('../src/routines/usage');

    const unused = {
      dependencies: ['a', 'c'],
      devdependencies: ['d', 'f']
    };

    printUnused('dependencies', unused.dependencies);

    expect(logSpy.getCall(0).args).eql(['Unused dependencies', 'yellow']);
    expect(logSpy.getCall(1).args).eql(['  - a', 'red']);
    expect(logSpy.getCall(2).args).eql(['  - c', 'red']);
  });

  it('printUnused() prints message when no unused are found', () => {
    const { printUnused } = require('../src/routines/usage');

    printUnused('dependencies', []);

    expect(logSpy.getCall(0).args).eql(['Unused dependencies', 'yellow']);
    expect(logSpy.getCall(1).args).eql([' No unused dependencies found', 'green']);
  });
});
