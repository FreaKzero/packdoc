/* istanbul ignore file */
const chai = require('chai');
const mockery = require('mockery');
const sinon = require('sinon');
process.env.NODE_ENV = 'test';

const expect = chai.expect;

const helpermock = {
  error: msg => msg,
  quit: msg => msg,
  writeJson: (file, json) => 1,
  log: msg => msg
};

const logspy = sinon.spy(helpermock, 'log');
const errorspy = sinon.spy(helpermock, 'error');
const quitspy = sinon.spy(helpermock, 'quit');

describe('routine/lint', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    mockery.registerMock('../helpers', helpermock);
  });
  afterEach(() => {
    logspy.resetHistory();
    mockery.resetCache();
    mockery.deregisterAll();
    mockery.disable();
  });

  it('flag list gives back rules', () => {
    const { lint } = require('../src/routines/lint');
    const { linterDefault } = require('../src/settings');

    const pkg = {
      name: 'test',
      version: '1.1.1,',
      packdoc: {
        manager: 'yarn'
      }
    };

    lint(
      {
        flags: {
          list: true
        }
      },
      pkg
    );
    expect(JSON.parse(logspy.getCall(0).args[0])).eql(linterDefault);
  });

  it('flag list gives back rules with customs', () => {
    const { lint } = require('../src/routines/lint');
    const { linterDefault } = require('../src/settings');

    const pkg = {
      name: 'test',
      version: '1.1.1,',
      packdoc: {
        manager: 'yarn',
        lint: {
          'license-type': 'off'
        }
      }
    };

    lint(
      {
        flags: {
          list: true
        }
      },
      pkg
    );

    linterDefault['license-type'] = 'off';

    expect(JSON.parse(logspy.getCall(0).args[0])).eql(linterDefault);
  });

  it('printBlock() prettyprints issues', () => {
    const { printBlock } = require('../src/routines/lint');
    const res = [
      {
        lintId: 'ID',
        lintMessage: 'a lint message',
        severity: 'error'
      },
      {
        lintId: 'ID1',
        lintMessage: 'a lint message',
        severity: 'error'
      },
      {
        lintId: 'ID2',
        lintMessage: 'another message',
        severity: 'warn'
      }
    ];

    printBlock(res, { flags: { strict: false } });

    expect(logspy.getCall(0).args).eql(['  a lint message (ID/error) ', 'red']);

    expect(logspy.getCall(1).args).eql(['  a lint message (ID1/error) ', 'red']);

    expect(logspy.getCall(2).args).eql(['  another message (ID2/warn) ', 'yellow']);

    expect(quitspy.called).eql(true);
  });

  it('printBlock() exits with 1 when strict flag is given', () => {
    const { printBlock } = require('../src/routines/lint');
    const res = [
      {
        lintId: 'ID',
        lintMessage: 'a lint message',
        severity: 'error'
      },
      {
        lintId: 'ID1',
        lintMessage: 'a lint message',
        severity: 'error'
      },
      {
        lintId: 'ID2',
        lintMessage: 'another message',
        severity: 'warn'
      }
    ];

    printBlock(res, { flags: { strict: true } });
    expect(errorspy.called).eql(true);
  });

  it('printBlock() doesnt crash when no cli object is given', () => {
    const { printBlock } = require('../src/routines/lint');
    const res = [
      {
        lintId: 'ID',
        lintMessage: 'a lint message',
        severity: 'error'
      },
      {
        lintId: 'ID1',
        lintMessage: 'a lint message',
        severity: 'error'
      },
      {
        lintId: 'ID2',
        lintMessage: 'another message',
        severity: 'warn'
      }
    ];

    printBlock(res);
    expect(quitspy.called).eql(true);
  });
});
