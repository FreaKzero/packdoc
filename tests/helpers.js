/* istanbul ignore file */
const chai = require('chai');
const mockery = require('mockery');
const sinon = require('sinon');

const expect = chai.expect;

let filereturn = `{"a": 1, "b": "2"}`;
let existreturn = true;

const fsmock = {
  readFileSync: (a) => {
    if (a.indexOf('package.json') > -1) {
      return filereturn;
    }
    return a;
  },
  writeFileSync: (a, b, c) => [a, b, c],
  existsSync: (a) => existreturn
};

const helpermock = {
  error: msg => msg,
  quit: msg => msg,
  writeJson: () => 1,
  log: msg => msg
};

const writeFileSyncSpy = sinon.spy(fsmock, 'writeFileSync');
const readFileSyncSpy = sinon.spy(fsmock, 'readFileSync');

describe('routine/helpers', () => {
  beforeEach(() => {
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    mockery.registerMock('fs', fsmock);
    mockery.registerMock('../helpers', helpermock);
  });
  afterEach(() => {
    writeFileSyncSpy.resetHistory();
    mockery.resetCache();
    mockery.deregisterAll();
  });

  it('getPath() should resolve from cwd', () => {
    const { getPath } = require('../src/helpers');
    process.cwd = () => '/a/b/c';
    const a = getPath('./f.json');
    expect(a).eql('/a/b/c/f.json');
  });

  it('quit() should exit with 0', () => {
    const { quit } = require('../src/helpers');
    process.exit = (code) => code;
    const procSpy = sinon.spy(process, 'exit');

    quit('some message');
    expect(procSpy.getCall(0).args).eql([0]);
  });

  it('error() should exit with 1', () => {
    const { error } = require('../src/helpers');
    process.exit = (code) => code;
    const procSpy = sinon.spy(process, 'exit');

    error('some message');
    expect(procSpy.getCall(0).args).eql([1]);
  });

  it('readFile() should read file with cwd', () => {
    const { readFile } = require('../src/helpers');
    const a = readFile('package-lock.json');
    expect(a).eql('/a/b/c/package-lock.json');
  });

  it('writeJson() should write JSON with cwd', () => {
    const { writeJson } = require('../src/helpers');
    writeJson('package-lock.json', { content: 'a' });
    expect(writeFileSyncSpy.getCall(0).args).eql([ '/a/b/c/package-lock.json', '{\n  "content": "a"\n}', 'utf8' ]);
  });

  it('getPackage() should return package.json mixed with default packdoc settings', () => {
    const { getPackage } = require('../src/helpers');
    const x = getPackage();
    const exp = {
      'a': 1,
      'b': '2',
      'packdoc': {
        'lint': {},
        'manager': 'yarn',
        'usageIgnore': [],
        'usageIgnoreDirs': []
      }
    };
    expect(x).eql(exp);
    expect(readFileSyncSpy.getCall(0).args).eql(['/a/b/c/package-lock.json', 'utf8']);
  });

  it('getPackage() already defined settings shouldnt be overwritten', () => {
    const { getPackage } = require('../src/helpers');
    filereturn = `{
      "a": 1,
      "b": "2",
      "packdoc": {
        "lint": {"some-rule": "off"},
        "manager": "npm"
      }
    }`;
    const x = getPackage();

    const exp = {
      'a': 1,
      'b': '2',
      'packdoc': {
        'lint': { 'some-rule': 'off' },
        'manager': 'npm',
        'usageIgnore': [],
        'usageIgnoreDirs': []
      }
    };
    expect(x).eql(exp);
    expect(readFileSyncSpy.getCall(0).args).eql(['/a/b/c/package-lock.json', 'utf8']);
  });

  it('exists() should exit with 1 when err parameter is not null', () => {
    const { exists } = require('../src/helpers');
    existreturn = false;
    process.exit = (code) => code;
    const procSpy = sinon.spy(process, 'exit');

    exists('.wat');
    expect(procSpy.getCall(0).args).eql([1]);
  });

  it('exists() should return boolean false when err parameter is null', () => {
    const { exists } = require('../src/helpers');
    existreturn = false;
    process.exit = (code) => code;

    const a = exists('.wat', null);
    expect(a).eql(false);
  });

  it('exists() should return boolean true when file exists', () => {
    const { exists } = require('../src/helpers');
    existreturn = true;
    process.exit = (code) => code;

    const a = exists('.wat', null);
    expect(a).eql(true);
  });
});
