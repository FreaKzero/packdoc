/* istanbul ignore file */
const chai = require('chai');
const mockery = require('mockery');

const expect = chai.expect;

const helpermock = {
  error: msg => msg,
  quit: msg => msg,
  writeJson: (file, json) => 1,
  log: msg => msg
};

describe('routine/fix', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    mockery.registerMock('../helpers', helpermock);
  });
  afterEach(() => {
    mockery.deregisterAll();
  });

  it('getNonPinned() should give back the right value', () => {
    const { getNonPinned } = require('../src/routines/fix');
    const pkg = {
      name: 'test',
      version: '0.0.0',
      dependencies: {
        a: '^1.1.1',
        b: '~0.0.1',
        c: '1.1.1'
      },
      devDependencies: {
        d: '^1.0.1',
        e: '~2.0.0',
        f: '1.1.1'
      },
      resolutions: {
        g: '~1.0.0',
        h: '^2.2.2',
        i: '3.3.3'
      }
    };
    const expected = [
      { version: '^1.1.1', name: 'a', dep: 'dependencies' },
      { version: '~0.0.1', name: 'b', dep: 'dependencies' },
      { version: '^1.0.1', name: 'd', dep: 'devDependencies' },
      { version: '~2.0.0', name: 'e', dep: 'devDependencies' },
      { version: '~1.0.0', name: 'g', dep: 'resolutions' },
      { version: '^2.2.2', name: 'h', dep: 'resolutions' }
    ];

    const f = getNonPinned(pkg, [
      'dependencies',
      'devDependencies',
      'resolutions'
    ]);
    expect(f).to.be.a('array');
    expect(f).to.eql(expected);
  });

  it('pinVersions() should pin Versions based on lock object and fails', () => {
    const { pinVersions } = require('../src/routines/fix');
    const pkg = {
      name: 'test',
      version: '0.0.0',
      dependencies: {
        a: '^1.1.1',
        b: '~0.0.1',
        c: '1.1.1'
      }
    };

    const lockedVersions = [
      {
        name: 'a',
        version: '9.9.9'
      },
      {
        name: 'b',
        version: '9.9.9'
      }
    ];

    const fails = [
      { version: '^1.1.1', name: 'a', dep: 'dependencies' },
      { version: '~0.0.1', name: 'b', dep: 'dependencies' }
    ];

    const expected = {
      name: 'test',
      version: '0.0.0',
      dependencies: {
        a: '9.9.9',
        b: '9.9.9',
        c: '1.1.1'
      }
    };

    pinVersions(pkg, lockedVersions, fails, 'dependencies');

    expect(pkg).to.eql(expected);
  });
  it('sortDependency() should sort dependencies', () => {
    const { sortDependency } = require('../src/routines/fix');
    const pkg = {
      name: 'test',
      version: '0.0.0',
      dependencies: {
        d: '9.0.9',
        b: '0.0.1',
        c: '1.1.1',
        a: '1.1.1'
      }
    };

    const expected = {
      name: 'test',
      version: '0.0.0',
      dependencies: { a: '1.1.1', b: '0.0.1', c: '1.1.1', d: '9.0.9' }
    };
    sortDependency(pkg, 'dependencies');
    expect(pkg).to.eql(expected);
  });
});
