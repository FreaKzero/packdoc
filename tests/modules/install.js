/* istanbul ignore file */
const chai = require('chai');
const mockery = require('mockery');
const sinon = require('sinon');
const expect = chai.expect;

const processmock = {
  spawn: (a, b) => ({
    on: (a, b) => a, b
  })
};

const oramock = (a) => ({
  start: () => 1,
  succeed: (c) => c,
  fail: (d) => d
});

const spawnSpy = sinon.spy(processmock, 'spawn');

describe('module/install', () => {
  beforeEach(() => {
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    mockery.registerMock('ora', oramock);
    mockery.registerMock('child_process', processmock);
  });
  afterEach(() => {
    mockery.resetCache();
    mockery.deregisterAll();
    spawnSpy.resetHistory();
  });

  it('exec() spawns manager command with given arguments', () => {
    const { exec } = require('../../src/modules/install');
    exec('yarn', ['install'], { load: 'a', success: 'a', error: 'a' });
    expect(spawnSpy.getCall(0).args).eql(['yarn', ['install']]);
  });

  it('install() should install given deps with manager setting', () => {
    const { install } = require('../../src/modules/install');

    const pkg = {
      packdoc: {
        manager: 'yarn'
      }
    };

    const deps = ['a', 'b', 'c'];

    install(pkg, deps);
    expect(spawnSpy.getCall(0).args).eql(['yarn', ['add',
      '-E',
      'a',
      'b',
      'c']]);
  });
  it('install() should just start install when no deps are given', () => {
    const { install } = require('../../src/modules/install');
    const pkg = {
      packdoc: {
        manager: 'yarn'
      }
    };
    install(pkg);
    expect(spawnSpy.getCall(0).args).eql(['yarn', ['install']]);
  });

  it('install() should exit with 1 when no manager is given', () => {
    const { install } = require('../../src/modules/install');
    process.exit = (code) => code;
    const procSpy = sinon.spy(process, 'exit');
    const pkg = {
      packdoc: {
      }
    };
    install(pkg);

    expect(procSpy.getCall(0).args).eql([1]);
  });
});
