/* istanbul ignore file */
const chai = require('chai');
const sinon = require('sinon');
const mockery = require('mockery');
const expect = chai.expect;
const { injectGetters } = require('../../src/modules/plugins');

let returnVersion = '1.1.1';
const processmock = {
  spawn: (cmd, param) => ({
    on: (event, onCb) => [event, onCb],
    stdout: {
      setEncoding: (enc) => enc,
      on: (outevent, outCb) => outCb(returnVersion)
    }
  })
};

const plugins = {
  engines: {
    node: {
      param: '-v',
      url: 'https://nodejs.org/en/download/releases/'
    },
    chrome: {
      param: '-uh',
      url: 'http://some.url'
    }
  }
};
plugins._ = injectGetters(plugins);

const pluginsMock = {
  getPlugins: () => (plugins)
};

const spawnSpy = sinon.spy(processmock, 'spawn');

describe('module/getEngines', () => {
  beforeEach(() => {
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    mockery.registerMock('child_process', processmock);
    mockery.registerMock('./plugins', pluginsMock);
  });
  afterEach(() => {
    mockery.resetCache();
    mockery.deregisterAll();
    spawnSpy.resetHistory();
  });

  it('getEngine() returns a Promise and resolves the correct version from spawn', done => {
    const { getEngine } = require('../../src/modules/getEngines');

    const plugins = {
      engines: {
        wat: {
          param: '--version',
          url: 'http://some.url'
        }
      }
    };

    plugins._ = injectGetters(plugins);

    getEngine('wat', plugins).then(a => {
      expect(a).eql('1.1.1');
      done();
    }).catch(e => {
      console.log(e);
      done();
    });
  });

  it('getEngine() uses special stringtransformers and parameters from settings', done => {
    const { getEngine } = require('../../src/modules/getEngines');
    returnVersion = 'some version v3.2.1';

    const plugins = {
      engines: {
        wat: {
          param: '--wut',
          url: 'http://some.url',
          transform: (value) => value.split(' ')[2] || null
        }
      }
    };

    plugins._ = injectGetters(plugins);

    getEngine('wat', plugins).then(a => {
      expect(a).eql('v3.2.1');
      expect(spawnSpy.getCall(0).args).eql([ 'wat', [ '--wut' ] ]);
      done();
    }).catch(e => {
      console.log(e);
      done();
    });
  });

  it('getEnginePromise() works with and without plugins', done => {
    const { getEnginesPromise } = require('../../src/modules/getEngines');
    returnVersion = '1.1.1';

    const pkg = {
      engines: {
        'wat': '1.1.1',
        'rly': '2.2.2',
        'node': '3.3.3'
      }
    };

    const exp = [ { name: 'wat',
      has: '1.1.1',
      should: '1.1.1',
      satisfied: true,
      url: null },
    { name: 'rly',
      has: '1.1.1',
      should: '2.2.2',
      satisfied: false,
      url: null },
    { name: 'node',
      has: '1.1.1',
      should: '3.3.3',
      satisfied: false,
      url: 'https://nodejs.org/en/download/releases/' } ];

    const plugs = {
      engines: {
        node: {
          param: '-v',
          url: 'https://nodejs.org/en/download/releases/'
        },
        chrome: {
          param: '-uh',
          url: 'http://some.url'
        }
      }
    };

    plugs._ = injectGetters(plugs);

    getEnginesPromise(pkg, plugs).then(items => {
      expect(items).eql(exp);
      done();
    });
  });

  it('getEnginePromise() gracefully returns ?.?.? if version is not resolveable', done => {
    const { getEnginesPromise } = require('../../src/modules/getEngines');
    returnVersion = 'some version x.3.f_adsf';

    const pkg = {
      engines: {
        'node': '1.1.1'
      }
    };
    const plugs = {
      engines: {
        node: {
          param: '-v',
          url: 'https://nodejs.org/en/download/releases/'
        },
        chrome: {
          param: '-uh',
          url: 'http://some.url'
        }
      }
    };
    plugs._ = injectGetters(plugs);
    const exp = [ { name: 'node',
      has: '?.?.?',
      should: '1.1.1',
      satisfied: false,
      url: 'https://nodejs.org/en/download/releases/',
      error: 'TypeError: Invalid Version: some version x.3.f_adsf' } ];

    getEnginesPromise(pkg, plugs).then(items => {
      expect(items).eql(exp);
      done();
    }).catch(e => {
      console.log(e);
      throw e;
      done();
    });
  });
});
