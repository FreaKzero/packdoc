/* istanbul ignore file */
const chai = require('chai');
const mockery = require('mockery');
const sinon = require('sinon');

const expect = chai.expect;

const npmlock = `{
    "name": "packdoc",
    "version": "0.0.0-development.0",
    "lockfileVersion": 1,
    "requires": true,
    "dependencies": {
      "@babel/code-frame": {
        "version": "7.0.0",
        "resolved": "https://registry.npmjs.org/@babel/code-frame/-/code-frame-7.0.0.tgz",
        "integrity": "sha512-OfC2uemaknXr87bdLUkWog7nYuliM9Ij5HUcajsVcMCpQrcLmtxRbVFTIqmcSkSeYRBFBRxs2FiUqFJDLdiebA=="
      },
      "@babel/generator": {
        "version": "7.4.0",
        "resolved": "https://registry.npmjs.org/@babel/generator/-/generator-7.4.0.tgz",
        "integrity": "sha512-/v5I+a1jhGSKLgZDcmAUZ4K/VePi43eRkUs3yePW1HB1iANOD5tqJXwGSG4BZhSksP8J9ejSlwGeTiiOFZOrXQ=="
      },
      "@babel/helper-function-name": {
        "version": "7.1.0",
        "resolved": "https://registry.npmjs.org/@babel/helper-function-name/-/helper-function-name-7.1.0.tgz",
        "integrity": "sha512-A95XEoCpb3TO+KZzJ4S/5uW5fNe26DjBGqf1o9ucyLyCmi1dXq/B3c8iaWTfBk3VvetUxl16e8tIrd5teOCfGw=="
      }
  }
}`;

const yarnlock = {
  'type': 'success',
  'object': {
    'a@^7.0.0': {
      'version': '7.0.0',
      'resolved': 'https://registry.yarnpkg.com/@babel/code-frame/-/code-frame-7.0.0.tgz#06e2ab19bdb535385559aabb5ba59729482800f8',
      'dependencies': {
        '@babel/highlight': '^7.0.0'
      }
    },
    'b@^7.0.0': {
      'version': '7.4.0',
      'resolved': 'https://registry.yarnpkg.com/@babel/generator/-/generator-7.4.0.tgz#c230e79589ae7a729fd4631b9ded4dc220418196',
      'dependencies': {
        '@babel/types': '^7.4.0',
        'jsesc': '^2.5.1',
        'lodash': '^4.17.11',
        'source-map': '^0.5.0',
        'trim-right': '^1.0.1'
      }
    },
    'c@^7.4.0': {
      'version': '7.4.0',
      'resolved': 'https://registry.yarnpkg.com/@babel/generator/-/generator-7.4.0.tgz#c230e79589ae7a729fd4631b9ded4dc220418196',
      'dependencies': {
        '@babel/types': '^7.4.0',
        'jsesc': '^2.5.1',
        'lodash': '^4.17.11',
        'source-map': '^0.5.0',
        'trim-right': '^1.0.1'
      }
    }
  }
};

let readfilereturn = 1;
const yarnmock = {
  parse: (file) => yarnlock
};

const fsmock = {
  existsSync: () => true,
  readFileSync: () => readfilereturn
};

describe('module/locks', () => {
  beforeEach(() => {
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    mockery.registerMock('@yarnpkg/lockfile', yarnmock);
    mockery.registerMock('fs', fsmock);
  });
  afterEach(() => {
    mockery.resetCache();
    mockery.deregisterAll();
  });

  it('getLockObject() should return yarn.lock object without the type', () => {
    const { getLockObject } = require('../../src/modules/locks');

    const x = getLockObject('yarn');
    expect(x).to.be.an('object');
    expect(x.type).eql(undefined);
    expect(Object.keys(x).length).to.be.greaterThan(0);
  });

  it('getLockObject() should return npm object', () => {
    const { getLockObject } = require('../../src/modules/locks');
    readfilereturn = npmlock;
    const x = getLockObject('npm');

    expect(x).to.be.an('object');
    expect(x.name).eql(undefined);
    expect(x.version).eql(undefined);
    expect(x.lockfileVersion).eql(undefined);
    expect(x.required).eql(undefined);
    expect(Object.keys(x).length).to.be.greaterThan(0);
  });

  it('getLockObject() exits with 1 when wrong manger is given', () => {
    const { getLockObject } = require('../../src/modules/locks');
    readfilereturn = npmlock;
    process.exit = (c) => c;
    const procSpy = sinon.spy(process, 'exit');
    getLockObject('asdf');
    expect(procSpy.getCall(0).args).eql([1]);
  });

  it('getLocks() should return locks with specific data structure', () => {
    const { getLocks } = require('../../src/modules/locks');
    readfilereturn = yarnlock;
    const x = getLocks('yarn');
    expect(x).eql([ { version: '7.0.0', name: 'a' },
      { version: '7.4.0', name: 'b' },
      { version: '7.4.0', name: 'c' } ]);

    readfilereturn = npmlock;
    const y = getLocks('npm');
    expect(y).eql([ { version: '7.0.0', name: '@babel/code-frame' },
      { version: '7.4.0', name: '@babel/generator' },
      { version: '7.1.0', name: '@babel/helper-function-name' } ]);
  });

  it('getName() with yarn should give back the right value', () => {
    const { getName } = require('../../src/modules/locks');
    const name = getName('yarn', '@yarnpkg/lockfile@^1.1.0');
    expect(name).be.a('string');
    expect(name).to.equal('@yarnpkg/lockfile');
  });

  it('getName() with npm should give back the right value', () => {
    const { getName } = require('../../src/modules/locks');
    const name = getName('npm', '@yarnpkg/lockfile');

    expect(name).be.a('string');
    expect(name).to.equal('@yarnpkg/lockfile');
  });
});

// it('getLocks() with yarn should return formatted object', () => {
//   const { getLocks } = require('../src/routines/fix');
//   const locks = getLocks('yarn');
//   expect(locks).eql([
//     { version: '7.0.0', name: '@babel/code-frame' },
//     { version: '7.4.0', name: '@babel/generator' },
//     { version: '7.1.0', name: '@babel/helper-function-name' }
//   ]);
// });
