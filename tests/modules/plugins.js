/* istanbul ignore file */
const chai = require('chai');
const mockery = require('mockery');
const expect = chai.expect;

const returnRequire = {
  engines: {
    chrome: {
      param: '-version',
      url: 'https://nodejs.org/en/download/releases/'
    }
  }
};

const returnExists = true;

const settingsmock = {
  enginesDefault: {
    node: {
      param: '-v',
      url: 'https://nodejs.org/en/download/releases/'
    }
  }
};

const helpermock = {
  exists: (path) => returnExists,
  __scriptRequire: (path) => returnRequire,
  getPath: (path) => path
};

describe('module/plugins', () => {
  beforeEach(() => {
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    mockery.registerMock('../helpers', helpermock);
    mockery.registerMock('../settings', settingsmock);
  });
  afterEach(() => {
    mockery.resetCache();
    mockery.deregisterAll();
  });

  it('getPlugins() returns merged enginesettings with engine plugins', () => {
    const { getPlugins } = require('../../src/modules/plugins');
    const f = getPlugins();
    const exp = {
      engines: {
        chrome: { param: '-version',
          url: 'https://nodejs.org/en/download/releases/'
        },
        node: {
          param: '-v',
          url: 'https://nodejs.org/en/download/releases/'
        }
      }
    };

    expect(f).eql(exp);
  });
});
