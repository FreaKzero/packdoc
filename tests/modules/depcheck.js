/* istanbul ignore file */
const chai = require('chai');
const expect = chai.expect;

describe('module/depcheck', () => {
  it('resolveResolutions() should resolve resolutions', () => {
    const { resolveResolutions } = require('../../src/modules/depcheck');


    const missing = {
      'babel': ['a/b/c'],
      'a': ['a/b/c'],
      'c': ['a/b/c']
    };

    const pkg = {
      resolutions: {
        'a': '1.1.1'
      }
    };

    const x = resolveResolutions(pkg, missing);
    const exp = [
      {
        'dep': 'babel',
        'paths': [
          'a/b/c'
        ]
      },
      {
        'dep': 'c',
        'paths': [
          'a/b/c'
        ]
      }
    ];
    expect(x).eql(exp);
  });
});
