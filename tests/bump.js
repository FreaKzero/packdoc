/* istanbul ignore file */
const chai = require('chai');
const mockery = require('mockery');
const sinon = require('sinon');

const expect = chai.expect;

const helpermock = {
  error: msg => msg,
  quit: msg => msg,
  writeJson: (file, json) => [file, json],
  log: msg => msg
};

const writeSpy = sinon.spy(helpermock, 'writeJson');
const errorSpy = sinon.spy(helpermock, 'error');
const quitSpy = sinon.spy(helpermock, 'quit');
const logSpy = sinon.spy(helpermock, 'log');

describe('routine/bump', () => {
  beforeEach(() => {
    mockery.enable({
      useCleanCache: true,
      warnOnReplace: false,
      warnOnUnregistered: false
    });
    mockery.registerMock('../helpers', helpermock);
  });
  afterEach(() => {
    quitSpy.resetHistory();
    errorSpy.resetHistory();
    quitSpy.resetHistory();
    logSpy.resetHistory();
    writeSpy.resetHistory();

    mockery.resetCache();
    mockery.deregisterAll();
  });

  it('Should write version in package.json', () => {
    const bump = require('../src/routines/bump');
    const cli = {
      input: [null, '2.2.2'],
      flags: {}
    };

    const pkg = {
      name: 'test',
      version: '0.0.0'
    };
    bump(cli, pkg);

    expect(writeSpy.getCall(0).args).eql(['./package.json', {
      name: 'test',
      version: '2.2.2'
    }]);
  });

  it('Should prevent versioning when its not SEMVER compatible', () => {
    const bump = require('../src/routines/bump');

    const cli = {
      input: [null, 'x.f-1.2n'],
      flags: {}
    };

    const pkg = {
      name: 'test',
      version: '0.0.0'
    };
    bump(cli, pkg);

    expect(errorSpy.getCall(0).args).eql([`Version ${cli.input[1]} is not SEMVER conform`]);
  });

  it('loose enables loose versioning', () => {
    const bump = require('../src/routines/bump');

    const cli = {
      input: [null, 'x.f-1.2n'],
      flags: { loose: true }
    };

    const pkg = {
      name: 'test',
      version: '0.0.0'
    };
    bump(cli, pkg);
    expect(logSpy.getCall(0).args).eql([`Versioning test with version ${cli.input[1]}`, 'yellow']);
    expect(quitSpy.getCall(0).args).eql([`Successfully Versioned ${pkg.name} with ${cli.input[1]}`, 'green']);
  });

  it('check should not write package.json', () => {
    const bump = require('../src/routines/bump');

    const cli = {
      input: [null, '1.1.1'],
      flags: { check: true }
    };

    const pkg = {
      name: 'test',
      version: '0.0.0'
    };
    bump(cli, pkg);

    expect(quitSpy.getCall(0).args).eql(['Version 1.1.1 is SEMVER conform', 'green']);
    expect(writeSpy.called).eql(false);
  });

  it('check with wrong version should exit with 1', () => {
    const bump = require('../src/routines/bump');

    const cli = {
      input: [null, '123jdfs.123ä'],
      flags: { check: true }
    };

    const pkg = {
      name: 'test',
      version: '0.0.0'
    };

    bump(cli, pkg);
    expect(errorSpy.getCall(0).args).eql(['Version 123jdfs.123ä is not SEMVER conform']);
    expect(writeSpy.called).eql(false);
  });

  // TODO: test with dir
});
