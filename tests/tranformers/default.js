/* istanbul ignore file */
const { defaultTransformer } = require('../../src/modules/getEngines');
const chai = require('chai');

const expect = chai.expect;

const outputs = {
  yarn: '6.6.6',
  node: '6.6.6',
  npm: '6.6.6',
  java: `java version "6.6.6_151"
  Java(TM) SE Runtime Environment (build 6.6.6_151-b12)
  Java HotSpot(TM) 64-Bit Server VM (build 25.151-b12, mixed mode)
  `,
  javac: `javac 6.6.6_151"`,
  mvn: `Apache Maven 6.6.6 (97c98ec64a1fdfee7767ce5ffb20918da4f719f3; 2018-10-24T20:41:47+02:00)
  Maven home: /usr/local/Cellar/maven/3.6.0/libexec
  Java version: 1.8.0_151, vendor: Oracle Corporation, runtime: /Library/Java/JavaVirtualMachines/jdk1.8.0_151.jdk/Contents/Home/jre
  Default locale: de_AT, platform encoding: UTF-8
  OS name: "mac os x", version: "10.14.3", arch: "x86_64", family: "mac"
  `,
  python: `Python 6.6.6`,
  python3: `Python 6.6.6`,
  pip: `pip 6.6.6 from /usr/local/lib/python2.7/site-packages/pip (python VERSION)`,
  pip3: 'pip 6.6.6 from /usr/local/lib/python2.7/site-packages/pip (python VERSION)',
  git: 'git version 6.6.6'
};

const expectedversion = '6.6.6';

describe('transformers/default', () => {
  it('default transformer works on all plugins without transformers', () => {
    const { enginesDefault } = require('../../src/settings');

    Object.keys(enginesDefault).forEach(engine => {
      expect(defaultTransformer(outputs[engine])).eql(expectedversion);
    });
  });
});
