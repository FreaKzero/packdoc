const path = require('path');
const pkg = require(path.resolve(__dirname, '../package.json'));

const version = pkg.version;

const usage = `${pkg.name} ${pkg.version}   <${pkg.repository}>
  ${pkg.description} 

Usage

  $ packdoc usage
    Check unused and missing dependencies
  Options  
    --strict, -s         Strict, exits with 1 when missing or unused deps are found

  $ packdoc lint
    package.json linting
  Options  
    --strict, -s         Strict, exits with 1 when linting issues are found
    --list, -l           Show list of current applied linting rules

  $ packdoc fix
    Fix issues in package.json - omit all options to fix everything
  Options
    --pin, -p            Pins versions based on lockfile
    --sort, -s           Sorts dependencies alphabetically
    --install, -i        Install missing dependencies 

  $ packdoc audit
    Runs engine validation, usage and <manager> audit command
  Options
    --strict,s           Strict, exits with 1 when audit issues are found

  $ packdoc engines
    Check and give help for engines entry in package.json
    see ${pkg.repository} for plugin development
  Options
    --strict, -s         Strict, exits with 1 when engine issues are found
    --list, -s           Show list of registered engine validators (also plugins)

  $ packdoc publish <semver> --token=<token>
    CI helper to publish packages to a registry (always semver strict)
    semver check -> overwrite package.version -> generating ~/.npmrc -> npm publish
  Options  
    --token, -t          authToken for your registry (required)
    --registry, -r       Registry where to publish (default: //registry.npmjs.org)

  $ packdoc bump <semver>
    Tool for versioning (overwrite) package.json or checking semver without writing
  Options
    --check, -c          Check if given version is SEMVER conform without writing to package.json
    --loose, -l          loose versioning
  
  Setup Example

  {
    "packdoc": {
      // linting rules overwrite, default []
      // rule definition documentation: https://github.com/tclindner/npm-package-json-lint/wiki
      "lint": {'require-homepage': 'off'},
      // ignore false-positives for packdoc usage command, default []
      "usageIgnore": ["@babel/*", "*eslint*"],
      // ignore directories for packdoc usage command, default []
      "usageIgnoreDirs": ['build', 'dist'],
      // npm or yarn, default yarn
      "manager": "yarn"
    }
  }
`;

const flags = {
  check: {
    type: 'boolean',
    alias: 'c'
  },
  strict: {
    type: 'boolean',
    alias: 's'
  },
  list: {
    type: 'boolean',
    alias: 'l'
  },
  install: {
    type: 'boolean',
    alias: 'i'
  },
  nobackup: {
    type: 'boolean',
    alias: 'n'
  },
  loose: {
    type: 'boolean',
    alias: 'l'
  },
  sort: {
    type: 'boolean',
    alias: 's'
  },
  pin: {
    type: 'boolean',
    alias: 'p'
  },
  token: {
    type: 'string',
    alias: 't'
  },
  registry: {
    type: 'string',
    alias: 'r'
  }
};

module.exports = {
  usage,
  version,
  flags
};
