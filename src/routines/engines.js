const { getEnginesPromise } = require('../modules/getEngines');
const { log, quit, error } = require('../helpers');
const ora = require('ora');

const engines = (cli, pkg, plugins) => {
  if (!pkg.engines || !Object.keys(pkg.engines).length) {
    quit('No Engines entry in package.json');
  }

  if (cli.flags.list) {
    log(`Currently registered engine validators:`, 'yellow');
    log(Object.keys(plugins).join(' '));
    log('');
    quit('Done');
  }

  const spinner = ora({ spinner: 'circle', text: 'Checking Engine versions' }).start();
  const all = getEnginesPromise(pkg, plugins);

  return all.then(items => {
    spinner.stop();
    log(`Engine validation`, 'yellow');
    let errorCount = 0;

    items.sort((a, b) => a.satisfied < b.satisfied).forEach(item => {
      if (item.satisfied) {
        log(`  ${item.name}\t installed ${item.has} \trequired ${item.should}`, 'green');
      } else {
        errorCount++;
        log(`  ${item.name}\t installed ${item.has} \trequired ${item.should}`, 'red');
        item.url ? log(`  see ${item.url} for updates`) : log(`  No Plugin for ${item.name} found`);
      }
    });

    if (cli.flags.strict && errorCount > 0) {
      log('');
      error(`Engine check found ${errorCount} unsatisfied engines`);
    }
  });
};

module.exports = {
  engines
};
