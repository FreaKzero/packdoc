const { valid } = require('semver');
const { homedir } = require('os');
const { writeFile, writeJson, log, error } = require('../helpers');
const path = require('path');

const { exec } = require('child_process');

const execPublish = () => {
  exec('npm publish', (error, stdout, stderr) => {
    if (error) {
      console.error(error);
      return;
    }
    console.log(stdout);
    console.log(stderr);
  });
};

const publish = (cli, pkg) => {
  const VERSION = cli.input[1];
  const defaultRegistry = '//registry.npmjs.org';

  if (!valid(VERSION)) {
    error(`Version ${VERSION} is not SEMVER conform`);
  }

  if (!cli.flags.token) {
    error('Please provide a NPM Token with --token=<TOKEN>');
  }

  if (!cli.flags.registry) {
    log(`no --registry flag given, using ${defaultRegistry}`, 'yellow');
  }

  pkg.version = VERSION;
  log(`Versioning ${pkg.name} to ${pkg.version}`, 'yellow');
  writeJson('./package.json', pkg);

  const registry = '/' + path.resolve(cli.flags.registry || defaultRegistry, ':_authToken=');
  const npmrc = `${homedir()}/.npmrc`;
  log(`Writing .npmrc file with registry and token to ${npmrc}`);
  writeFile(npmrc, `${registry}${cli.flags.token}`);
  execPublish();
};

module.exports = {
  publish
};
