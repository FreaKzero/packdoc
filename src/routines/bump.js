const semver = require('semver');
const { log, quit, error, writeJson } = require('../helpers');

const bump = (cli, pkg) => {
  const VERSION = cli.input[1];

  if (!cli.flags.loose && !semver.valid(VERSION)) {
    error(`Version ${VERSION} is not SEMVER conform`);
  }

  log(`Versioning ${pkg.name} with version ${VERSION}`, 'yellow');

  if (cli.flags.check) {
    return quit(
      `Version ${VERSION} is SEMVER conform`,
      'green'
    );
  }
  pkg.version = VERSION;
  writeJson('./package.json', pkg);
  return quit(
    `Successfully Versioned ${pkg.name} with ${pkg.version}`,
    'green'
  );
};

module.exports = bump;
