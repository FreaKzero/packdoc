const { spawn } = require('child_process');
const { quit, error } = require('../helpers');

const execAudit = (manager) => new Promise((resolve, reject) => {
  const child = spawn(manager, ['audit']);
  child.stdout.setEncoding('utf8');

  child.on('error', (err) => {
    console.log(err);
  });

  child.stderr.on('data', (data) => {
    console.log(data);
  });

  child.stdout.on('data', (data) => {
    console.log(data);
  });

  child.on('exit', (code) => code > 0 ? reject(new Error(code)) : resolve(1));
});

const audit = (cli, pkg) => {
  execAudit(pkg.packdoc.manager).then(a => {
    quit('done');
  }).catch(code => {
    cli.flags.strict ? error('done') : quit('done');
  });
};

module.exports = {
  audit,
  execAudit
};
