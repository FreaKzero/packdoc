const NpmPackageJsonLint = require('npm-package-json-lint').NpmPackageJsonLint;
const { log, quit, error } = require('../helpers');
const { linterDefault } = require('../settings');

const printBlock = (res, cli) => {
  const hasFlags = cli && cli.flags;
  let exit = 0;
  if (!res.length) {
    quit('No Linting issues');
  }

  res.forEach(item => {
    if (item.severity === 'error') {
      exit = 1;
    }
    log(
      `  ${item.lintMessage} (${item.lintId}/${item.severity}) `,
      item.severity === 'error' ? 'red' : 'yellow'
    );
  });

  return exit > 0 && hasFlags && cli.flags.strict
    ? error('Found linting issues')
    : quit('found linting issues');
};

const lint = (cli, pkg) => {
  const options = {
    ...linterDefault,
    ...pkg.packdoc.lint
  };

  if (cli.flags.list) {
    log(JSON.stringify(options, null, 2));
    quit('Done');
  } else {
    const npmPackageJsonLint = new NpmPackageJsonLint();
    const results = npmPackageJsonLint.lint(pkg, options);
    printBlock(results.issues, cli);
  }
};

module.exports = { lint, printBlock };
