const { log, writeJson } = require('../helpers');
const { depcheck } = require('../modules/depcheck');
const { getLocks } = require('../modules/locks');
const { install } = require('../modules/install');
const ora = require('ora');

const getNonPinned = (pkg, dependencyKey) => {
  const fails = [];
  for (const dep in dependencyKey) {
    const p = pkg[dependencyKey[String(dep)]];
    for (const idx in p) {
      const pack = {
        version: p[String(idx)],
        name: idx,
        dep: dependencyKey[String(dep)]
      };

      if (!/^[0-9]/.test(pack.version)) {
        fails.push(pack);
      }
    }
  }
  return fails;
};

const pinVersions = (pkg, lockedVersions, fails, depkey) => {
  if (pkg[String(depkey)]) {
    Object.keys(pkg[String(depkey)]).forEach(depname => {
      const bad = fails.find(faileddep => faileddep.name === depname);
      if (bad) {
        const found = lockedVersions.find(dep => bad.name === dep.name);
        if (found) {
          pkg[String(depkey)][bad.name] = found.version;
          log(
            `  - Pinned ${bad.name} from ${bad.version} to ${found.version}`,
            'green'
          );
        } else {
          log(
            `  - Cant find locked version for ${bad.name} version ${
              bad.version
            }`,
            'red'
          );
        }
      }
    });
  }
  return pkg;
};

const sortDependency = (pkg, depkey) => {
  if (pkg[String(depkey)]) {
    const deps = Object.assign({}, pkg[String(depkey)]);

    const sortedKeys = Object.keys(deps).sort((a, b) =>
      a < b ? -1 : a > b ? 1 : 0
    );

    const newDeps = {};

    sortedKeys.forEach(dep => {
      newDeps[String(dep)] = pkg[String(depkey)][String(dep)];
    });

    pkg[String(depkey)] = newDeps;
  }
};

const fix = (cli, pkg) => {
  const noFlag = (!cli.flags.pin && !cli.flags.sort && !cli.flags.install);

  if (noFlag) {
    log('No Flags are given, packdoc will pin, sort and install missing dependencies', 'yellow');
  }

  if (cli.flags.pin || noFlag) {
    const lock = getLocks(pkg.packdoc.manager);
    const fails = getNonPinned(pkg, [
      'dependencies',
      'devDependencies',
      'resolutions'
    ]);
    pinVersions(pkg, lock, fails, 'dependencies');
    pinVersions(pkg, lock, fails, 'devDependencies');
    pinVersions(pkg, lock, fails, 'resolutions');
  }

  if (cli.flags.sort || noFlag) {
    sortDependency(pkg, 'dependencies');
    sortDependency(pkg, 'devDependencies');
    sortDependency(pkg, 'resolutions');
  }

  if (cli.flags.install || noFlag) {
    const spinner = ora({ text: 'Checking Source for missing and unused dependencies', spinner: 'circle' }).start();
    depcheck(pkg).then(res => {
      spinner.stop();
      const deps = res.missing.map(item => item.dep);
      install(pkg, deps);
    });
  } else {
    install(pkg);
  }

  writeJson('package.json', pkg);
};

module.exports = {
  fix,
  getLocks,
  getNonPinned,
  pinVersions,
  sortDependency
};
