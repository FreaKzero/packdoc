
const { log } = require('../helpers');
const { depcheck } = require('../modules/depcheck');
const ora = require('ora');

const printMissing = (missing) => {
  log('Missing Dependencies', 'yellow');

  if (!missing.length) {
    return log(' No missing Dependencies found', 'green');
  }

  missing.forEach(dep => {
    log(`  Missing dependency found: ${dep.dep}`, 'red');
    dep.paths.forEach(filepath => {
      log(`  - ${filepath}`);
    });
  });
};

const printUnused = (kindDev, arr) => {
  log(`Unused ${kindDev}`, 'yellow');
  if (!arr.length) {
    return log(` No unused ${kindDev} found`, 'green');
  }
  arr.forEach(str => {
    log(`  - ${str}`, 'red');
  });
};

const usage = (cli, pkg, plugin) => {
  const spinner = ora({ text: 'Checking Source for missing and unused dependencies', spinner: 'circle' }).start();
  return depcheck(pkg, plugin).then(res => {
    spinner.stop();
    printMissing(res.missing);
    log('');
    printUnused('dependencies', res.dependencies);
    log('');
    printUnused('devdependencies', res.devDependencies);
  });
};

module.exports = { usage, printMissing, printUnused };
