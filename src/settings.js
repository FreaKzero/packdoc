const mainDefault = {
  usageIgnoreDirs: [],
  usageIgnore: [],
  manager: 'yarn',
  lint: {}
};

const depcheckDefault = (depchecklib, pkg) => ({
  ignoreBinPackage: false,
  skipMissing: false,
  ignoreDirs: pkg.packdoc && pkg.packdoc.usageIgnoreDirs,
  ignoreMatches: pkg.packdoc && pkg.packdoc.usageIgnore,
  parsers: {
    '*.js': depchecklib.parser.es6,
    '*.jsx': depchecklib.parser.jsx
  },
  detectors: [
    depchecklib.detector.requireCallExpression,
    depchecklib.detector.importDeclaration
  ],
  specials: [depchecklib.special.webpack, depchecklib.special.babel]
});

const linterDefault = {
  'require-author': 'error',
  'require-description': 'error',
  'require-engines': 'error',
  'require-license': 'error',
  'require-name': 'error',
  'require-repository': 'error',
  'require-version': 'error',
  'require-bugs': 'error',
  'require-homepage': 'off',
  'require-keywords': 'error',
  'bin-type': 'error',
  'config-type': 'error',
  'description-type': 'error',
  'devDependencies-type': 'error',
  'directories-type': 'error',
  'engines-type': 'error',
  'files-type': 'error',
  'homepage-type': 'error',
  'keywords-type': 'error',
  'license-type': 'error',
  'main-type': 'error',
  'man-type': 'error',
  'name-type': 'error',
  'preferGlobal-type': 'error',
  'private-type': 'error',
  'repository-type': 'error',
  'scripts-type': 'error',
  'version-type': 'error',
  'valid-values-author': 'off',
  'valid-values-private': ['error', [false]],
  'name-format': 'error',
  'version-format': 'error',
  'no-absolute-version-dependencies': 'off',
  'no-absolute-version-devDependencies': 'off',
  'no-caret-version-dependencies': 'error',
  'no-tilde-version-dependencies': 'error',
  'no-caret-version-devDependencies': 'error',
  'no-tilde-version-devDependencies': 'error',
  'prefer-alphabetical-devDependencies': 'warn',
  'prefer-alphabetical-dependencies': 'warn'
};

const enginesDefault = {
  yarn: {
    param: '-v',
    url: 'https://yarnpkg.com/en/docs/install'
  },
  node: {
    param: '-v',
    url: 'https://nodejs.org/en/download/releases/'
  },
  npm: {
    param: '-v',
    url: 'https://nodejs.org/en/download/releases/'
  },
  git: {
    param: '--version',
    url: 'https://git-scm.com/downloads',
    transform: (value) => value.split(' ')[2]
  },
  java: {
    param: '-version',
    url: 'https://java.com/en/download/'
  },
  javac: {
    param: '-version',
    url: 'https://java.com/en/download/'
  },
  mvn: {
    param: '-version',
    url: 'https://java.com/en/download/'
  },

  python: {
    param: '-V',
    url: 'https://java.com/en/download/'
  },

  python3: {
    param: '-V',
    url: 'https://java.com/en/download/'
  },

  pip: {
    param: '-V',
    url: 'https://java.com/en/download/'
  },
  pip3: {
    param: '-V',
    url: 'https://java.com/en/download/'

  }
};

module.exports = {
  mainDefault,
  linterDefault,
  enginesDefault,
  depcheckDefault
};
