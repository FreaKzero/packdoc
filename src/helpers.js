const chalk = require('chalk');
const fs = require('fs');
const path = require('path');
const { mainDefault } = require('./settings');
const getPath = relativepath => path.resolve(process.cwd(), relativepath);

const log = (msg, color = null) =>
  color ? console.log(chalk[String(color)](msg)) : console.log(msg);

const quit = msg => {
  log(`${msg}, exit with code 0`, 'green');
  process.exit(0);
};

const error = msg => {
  log(`${msg}, exit with code 1`, 'red');
  process.exit(1);
};

const readFile = (relativePath, enc = 'utf8') => {
  try {
    return fs.readFileSync(getPath(relativePath), enc);
  } catch (err) {
    error(err);
  }
};

const getObject = (relativePath, enc = 'utf8') =>
  // TODO: check file extension for JSON
  JSON.parse(readFile(relativePath))
;

const writeFile = (file, data) => {
  try {
    fs.writeFileSync(file, data, 'utf8');
  } catch (err) {
    error(err);
  }
};

const writeJson = (file, json) => {
  try {
    fs.writeFileSync(getPath(file), JSON.stringify(json, null, 2), 'utf8');
  } catch (err) {
    error(err);
  }
};
const exists = (relativePath, err = `${relativePath} does not exist`) => {
  if (!fs.existsSync(relativePath)) {
    if (err) {
      error(err);
    } else {
      return false;
    }
  }
  return true;
};

// TODO refactor
// check first if its there
const getPackage = () => {
  const pkg = getObject(getPath('./package.json'));
  return Object.assign(
    {},
    {
      ...pkg,
      packdoc: Object.assign({ ...mainDefault, ...pkg.packdoc })
    }
  );
};

const __scriptRequire = (path) => require(path);

module.exports = {
  log,
  getPath,
  quit,
  error,
  writeJson,
  writeFile,
  exists,
  readFile,
  getPackage,
  getObject,
  __scriptRequire
};
