const yarnlock = require('@yarnpkg/lockfile');
const { getObject, error, exists, readFile } = require('../helpers');

const getName = (manager, name) => {
  if (manager === 'npm') {
    return name;
  }
  const spl = name.split('@');
  return spl.length > 2 ? `@${spl[1]}` : spl[0];
};

const getLocks = manager => {
  const deps = getLockObject(manager);
  return Object.keys(deps).map(dep => ({
    version: deps[String(dep)].version,
    name: getName(manager, dep)
  }));
};

const getLockObject = manager => {
  let lock;
  switch (manager) {
    case 'yarn':
      exists('./yarn.lock', 'Manager is set to yarn but no yarn.lock file found');
      lock = yarnlock.parse(readFile('./yarn.lock'));
      return lock.object;

    case 'npm':
      exists(
        './package-lock.json',
        'Manager is set to npm but no package-lock file found'
      );
      lock = getObject('./package-lock.json');
      return lock.dependencies;
    default:
      error(`No manager support for ${manager}, please use yarn or npm`);
  }
};

module.exports = {
  getLockObject,
  getLocks,
  getName
};
