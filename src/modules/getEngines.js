const { spawn } = require('child_process');
const { satisfies } = require('semver');

const defaultTransformer = (value) => {
  const v = value.match(/\d{1,2}\.\d{1,2}\.\d{1,2}/ig);
  return v ? v[0] : null;
};

const getEngine = (engine, plugins) => new Promise((resolve, reject) => {
  const cmd = plugins._.getParam(engine);
  const transform = plugins._.getTransform(engine);

  const child = spawn(engine, [cmd]);
  child.stdout.setEncoding('utf8');
  child.on('error', (err) => {
    reject(err.message);
  });

  child.stdout.on('data', (version) => {
    resolve(transform(version) || '?.?.?');
  });
});

const getEnginesPromise = (pkg, plugins) => {
  const pendingPromises = Object.keys(pkg.engines).map(key => getEngine(key, plugins).then(version => ({
    name: key,
    has: version,
    should: pkg.engines[String(key)],
    satisfied: !!satisfies(version, pkg.engines[String(key)]),
    url: plugins._.getUrl(key)
  })).catch(e => ({
    name: key,
    has: '?.?.?',
    should: pkg.engines[String(key)],
    satisfied: false,
    url: plugins._.getUrl(key),
    error: e.toString()
  })));
  return Promise.all(pendingPromises);
};

module.exports = {
  getEngine,
  getEnginesPromise,
  defaultTransformer
};
