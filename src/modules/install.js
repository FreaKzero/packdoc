const { spawn } = require('child_process');
const ora = require('ora');
const { error } = require('../helpers');

const exec = (
  manager, params
) => new Promise((resolve, reject) => {
  const child = spawn(manager, params);

  child.on('error', (err) => {
    console.log(err);
    return reject(err);
  });

  child.on('exit', (code) => resolve(params));
});

const install = (pkg, deps = null) => {
  if (!pkg.packdoc.manager) {
    return error('No manager setting in config');
  }

  if (deps && deps.length) {
    const spinner = ora({ spinner: 'arc', text: `Installing missing dependencies with ${pkg.packdoc.manager}` }).start();
    exec(pkg.packdoc.manager, ['add', '-E'].concat(deps)).then(params => {
      const installed = params.splice(2, params.length).join(', ');
      spinner.succeed(`Installed dependencies: ${installed}`);
    }).catch(e => {
      spinner.fail(e);
    });
  }

  if (!deps) {
    const spinner = ora({ spinner: 'arc', text: `Running ${pkg.packdoc.manager} install` }).start();
    exec(pkg.packdoc.manager, ['install']).then(params => {
      spinner.succeed('Done');
    }).catch(e => {
      spinner.fail(e);
      process.exit(1);
    });
  }
};

module.exports = {
  install, exec
};
