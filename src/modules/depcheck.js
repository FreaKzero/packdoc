const depchecklib = require('depcheck');
const { depcheckDefault } = require('../settings');

const resolveResolutions = (pkg, missing) => {
  const missingkeys = Object.keys(missing);
  if (pkg.resolutions) {
    const resolutions = Object.keys(pkg.resolutions);

    return missingkeys.filter(str => resolutions.indexOf(str) < 0).map((i) => ({
      dep: i,
      paths: missing[String(i)]
    }));
  }
  return missingkeys.map((i) => ({
    dep: i,
    paths: missing[String(i)]
  }));
};

const depcheck = (pkg, plugin) => {
  const options = typeof plugin === 'function'
    ? plugin(depchecklib, pkg)
    : depcheckDefault(depchecklib, pkg);

  return new Promise((resolve, reject) => {
    depchecklib(process.cwd(), options, res => {
      res.missing = resolveResolutions(pkg, res.missing);
      return resolve(res);
    });
  });
};

module.exports = {
  depcheck,
  resolveResolutions
};
