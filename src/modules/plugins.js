const { exists, getPath, __scriptRequire } = require('../helpers');
const { enginesDefault } = require('../settings');

const injectGetters = (obj) => {
  const defaultTransformer = (v) => v;
  return {
    getParam: (key) => (obj.engines[String(key)] && obj.engines[String(key)].param) || '-v',
    getTransform: (key) => (obj.engines[String(key)] && obj.engines[String(key)].transform) || defaultTransformer,
    getUrl: (key) => (obj.engines[String(key)] && obj.engines[String(key)].url) || null
  };
};

const getPlugins = (plugin) => {
  const path = getPath('./packdoc-plugin.js');

  let plugins;
  if (exists(path, null)) {
    const plugs = __scriptRequire(path);
    plugins = Object.assign({}, {
      ...plugs,
      engines: {
        ...enginesDefault,
        ...plugs.engines
      }
    });
    return plugins;
  }

  plugins = Object.assign({}, {
    engines: {
      ...enginesDefault
    }
  });

  plugins._ = injectGetters(plugins);

  return plugins;
};

module.exports = {
  getPlugins,
  injectGetters
};
