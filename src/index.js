#!/usr/bin/env node
const meow = require('meow');
const { getPackage } = require('./helpers');
const routines = require('./routines/index');
const { flags, usage, version } = require('./manifest');
const { getPlugins } = require('./modules/plugins');

const cli = meow(usage, {
  flags,
  description: false
});

const pkg = getPackage();
const plugins = getPlugins();

switch (cli.input[0]) {
  case 'audit':
    routines.engines(cli, pkg, plugins).then(() => {
      console.log('');
      routines.usage(cli, pkg, plugins.depcheck || null).then(() => {
        console.log('');
        routines.audit(cli, pkg);
      });
    });
    break;
  case 'version':
    console.log(version);
    break;
  case 'publish':
    routines.publish(cli, pkg);
    break;
  case 'engines':
    routines.engines(cli, pkg, plugins);
    break;
  case 'bump':
    routines.bump(cli, pkg);
    break;
  case 'usage':
    routines.usage(cli, pkg, plugins.depcheck || null);
    break;
  case 'lint':
    routines.lint(cli, pkg);
    break;
  case 'fix':
    routines.fix(cli, pkg);
    break;
  default:
    console.log(cli.help);
    break;
}
